pub(crate) const HOST_OR_IP: &str = "127.0.0.1";
pub(crate) const PORT: &str = "9001";
pub(crate) const LOG_LEVEL: &str = "TRACE";
pub(crate) const MAX_CONNECTIONS: usize = 5;
