mod config;

use std::{env, io::Error};

use config::*;
use env_logger::Env;
use futures_util::{SinkExt, StreamExt};
use log::{error, info, trace};
use tokio::net::{TcpListener, TcpStream};
use tokio::sync::broadcast;
use tokio::sync::broadcast::{Receiver, Sender};
use tokio_tungstenite::tungstenite::Message;

#[tokio::main]
async fn main() -> Result<(), Error> {
    // initial Setup
    let _ = env_logger::Builder::from_env(Env::default().default_filter_or(LOG_LEVEL)).try_init();
    let addr = env::args()
        .nth(1)
        .unwrap_or_else(|| format!("{}:{}", HOST_OR_IP, PORT));
    let (tx, _) = broadcast::channel(MAX_CONNECTIONS);

    // Create the event loop and TCP listener we'll accept connections on.
    let try_socket = TcpListener::bind(&addr).await;
    let listener = try_socket.expect("Failed to bind");
    info!("Listening on: {}", addr);

    while let Ok((socket_stream, _)) = listener.accept().await {
        let tx = tx.clone();
        let rx = tx.subscribe();
        tokio::spawn(async move {
            accept_connection(socket_stream, tx, rx).await;
        });
    }

    Ok(())
}

type VSender = Sender<Message>;
type VReceiver = Receiver<Message>;
async fn accept_connection(socket_stream: TcpStream, tx: VSender, mut rx: VReceiver) {
    // upgrade to websocket
    let addr = socket_stream
        .peer_addr()
        .expect("connected streams should have a peer address");
    info!("Peer address: {}", addr);

    let ws_stream = tokio_tungstenite::accept_async(socket_stream)
        .await
        .expect("Error during the websocket handshake occurred");

    info!("New websocket connection: {}", addr);

    // receive data from the client
    let (mut write, mut read) = ws_stream.split();

    tokio::spawn(async move {
        while let Some(msg) = read.next().await {
            match msg {
                Ok(data) => match tx.send(data) {
                    Ok(data_size) => trace!("Data broadcast success: {}", data_size),
                    Err(e) => error!("Error sending data to receiver: {:?}", e),
                },
                Err(e) => {
                    //error!("Data Read Error");
                    panic!("Client data read error {:?}", e); // TODO
                }
            }
        }
    });

    // send the data to the client
    while let Ok(data) = rx.recv().await {
        match write.send(data).await {
            Ok(_) => trace!("Data successfully sent to client"),
            Err(e) => error!("Error sending data to client: {:?}", e), // TODO
        }
    }
}
